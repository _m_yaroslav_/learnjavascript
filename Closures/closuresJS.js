// замыкания - это способность функции JS запоминать лексическое окружение, в котором она была создана

// let x = 1;
//
// const logToConsole = function () {
//     console.log(x)
// }
// x = 2;
//
// logToConsole()
//
// x = 3;


// function makeCounter(count) {
//     return function () {
//         return count++;
//     };
// }
//
// let counter = makeCounter(0);
// let counter2 = makeCounter(0);
//
// alert( counter() ); //0
// alert( counter() ); //1
//
// alert( counter2() ); //0
// alert( counter2() ); //1


// параметры у функциях это переменные, даже если их нет
// function createIncrement() {
//     let count = 0;
//     function increment () {
//        count++;
//     }
//
//     // let massage = `Count is ${count}`; - при такой записи log будет = 0, потому что она захватывает значение count на момент создания, а не на момент вызова
//     function log() {
//          let massage = `Count is ${count}`;
//         console.log(massage);
//     }
//
//
//
//     return [increment, log]
// }
//
// const [increment, log] = createIncrement();
// increment();
// increment();
// increment();
// log();




function increment() {
    let count = 0
    return () => count++;
}

const create = increment()()
console.log(create)
increment()
