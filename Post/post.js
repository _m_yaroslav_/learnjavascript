const body = document.querySelector('body')




const API = 'https://jsonplaceholder.typicode.com'
const ENDPOINTS = Object.freeze({
    POSTS: "posts",
    USERS: "users",
    ALBUMS: "albums",


})

const NESTED_ENDPOINTS = Object.create({
    POSTS: "posts",
    ALBUMS: "albums",
    TODOS: "todos",
    PHOTOS: "photos",
    COMMENTS: "comments",
})

// fetch('https://jsonplaceholder.typicode.com/posts')
//     .then((response) => response.json())
//     .then((json) => console.log(json));


// fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
//     .then((response) => response.json())
//     .then((json) => console.log(json));


const getAllPosts = () => fetch(`${API}/${ENDPOINTS.POSTS}`)
    .then(response => response.json())

const getCommentById = (postId) => fetch(`${API}/${NESTED_ENDPOINTS.COMMENTS}?postId=${postId} `)
    .then(response => response.json())


const createPostCard = ({title, body, comments, id}) => {
    const wrapper = document.createElement('div');
    wrapper.className = 'post-card';
    wrapper.setAttribute('data-post-id', id);





    const commentsHTML = comments.map(({body, email}) =>
        `<li class="comment">   
            <p class="comment-author">Автор комментария: ${email}</p>
            <p class="comment-text">${body}</p> 
        </li>`
    ).join('');

    wrapper.addEventListener('click', (event) => {
        const isCommentExist = wrapper.querySelector('.comments-list')
        if (isCommentExist) {

            isCommentExist.remove()
        } else {

            wrapper.querySelector('.comments-section').insertAdjacentHTML('beforeend', `<ul class="comments-list">
            ${commentsHTML}
            })
            </ul>`)
        }


    })

    wrapper.insertAdjacentHTML('beforeend', `
        <h2 class="post-title">${title}</h2>
        <p class="post-content">${body}</p>

        <div class="comments-section">
            <h3>Комментарии: ${comments.length}</h3>
        </div>
    `);


    return wrapper;
};



const renderAllPosts = async () => {
    const allPosts = await getAllPosts()
    const commentsPromises = allPosts.map(({id}) => getCommentById(id))
    const comments = await Promise.all(commentsPromises)

    const mergeCommentsAndPosts = allPosts.map((post, index) => ({...post, comments: comments[index]}))

    console.log(mergeCommentsAndPosts)

    const postsCards = mergeCommentsAndPosts?.map(post => createPostCard(post))
    body.append(...postsCards)


}


renderAllPosts()




