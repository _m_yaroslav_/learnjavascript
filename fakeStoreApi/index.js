export const API = 'https://fakestoreapi.com';
export const ENDPOINTS = Object.freeze({
    PRODUCTS: '/products',
    CARTS: '/carts',
    USERS: '/users',
    AUTH: '/auth',
});

const NESTED_AUTH_ENDPOINT = Object.freeze({
    LOGIN: '/login'
});

export const NESTED_PRODUCTS_ENDPOINT = Object.freeze({
    CATEGORIES: 'categories',
    CATEGORY: 'category',
});

export const CATEGORIES = {
    electronics: "electronics",
    jewelery: "jewelery",
    "men's clothing": "men's clothing",
    "women's clothing": "women's clothing",
};


const main = document.querySelector('.products-container');
const body = document.querySelector('body');


let isUserLogin = false

let userProducts = null

let allProducts = null

// let localUserProducts = null

let userInfo = null

const modalWindowPosition = {
    right: 'right',
    center: 'center',

}

const sortOption = {
    rank: 'sort by rank',
    lowerPrice: 'sort by lower price',
    higherPrice: 'sort by higher price',
}

// Функция для отправки запросов на сервер
const sendRequest = async (url, method = "GET", option) => {
    const response = await fetch(url, {
        method,
        ...option
    });


    return await response.json();
};

// Функция для отображения ошибок
const displayError = (message) => {
    removeError();
    const errorDiv = document.createElement('div');
    errorDiv.className = 'error-message';
    errorDiv.textContent = message;

    // Кнопка для закрытия сообщения об ошибке
    const closeButton = document.createElement('button');
    closeButton.textContent = '×';
    closeButton.addEventListener('click', removeError);
    errorDiv.appendChild(closeButton);

    document.body.appendChild(errorDiv);
};


// Функция для удаления ошибок
const removeError = () => {
    const existError = document.querySelector('.error-message');
    if (existError) {
        existError.remove();
    }
};

// Функция для получения всех продуктов
const getAllProducts = async () => {
    try {
        removeError();
        return await sendRequest(`${API}${ENDPOINTS.PRODUCTS}`);
    } catch (error) {
        displayError('Не удалось загрузить продукты 😪. Пожалуйста, попробуйте позже.');
        console.error('Ошибка при получении продуктов', error);
    }
};


const initProducts = async () => {
    const serverProducts = await getAllProducts()
    allProducts = serverProducts
    sortProducts(allProducts)
}

document.addEventListener('DOMContentLoaded', initProducts)


// Функция для получения всех категорий
const getAllCategories = async () => {
    try {
        removeError();
        return await sendRequest(`${API}${ENDPOINTS.PRODUCTS}/${NESTED_PRODUCTS_ENDPOINT.CATEGORIES}`);
    } catch (error) {
        displayError('Не удалось загрузить категории 😔. Пожалуйста, попробуйте позже.');
        console.error('Ошибка при получении категорий', error);
    }
};

// Функция для получения продуктов по категории
const getProductsByCategory = async (category) => {
    try {
        removeError();
        return await sendRequest(`${API}${ENDPOINTS.PRODUCTS}/${NESTED_PRODUCTS_ENDPOINT.CATEGORY}/${category}`);
    } catch (error) {
        displayError(`Не удалось загрузить продукты для категории 😞 ${category}`);
        console.error('Ошибка при получении продуктов для категории', error);
    }
};

// Функция для получения продукта по ID
const getProductById = async (productId) => {
    try {
        removeError();
        return await sendRequest(`${API}${ENDPOINTS.PRODUCTS}/${productId}`);
    } catch (error) {
        displayError('Не удалось загрузить детали продукта 🙄. Пожалуйста, попробуйте позже.');
        console.error(`Ошибка при получении продукта с ID ${productId}`, error);
    }
};

// Функция для получения всех корзин
const getAllCarts = async () => {
    try {
        removeError();
        return await sendRequest(`${API}${ENDPOINTS.CARTS}`);
    } catch (error) {
        displayError('Не удалось загрузить корзины ☹️. Пожалуйста, попробуйте позже.');
        console.error('Ошибка при получении корзин', error);
    }
};

// Функция для получения всех пользователей
const getAllUsers = async () => {
    try {
        removeError();
        return await sendRequest(`${API}${ENDPOINTS.USERS}`);

    } catch (error) {
        displayError('Не удалось загрузить пользователей ☹️. Пожалуйста, попробуйте позже.');
        console.error('Ошибка при получении пользователей', error);

    }
};

const getSingleCart = async (id) => {
    try {
        removeError();
        return await sendRequest(`${API}${ENDPOINTS.CARTS}/${id}`);

    } catch (error) {
        displayError('Не удалось загрузить карту ☹️. Пожалуйста, попробуйте позже.');
        console.error('Ошибка при получении карты', error);

    }
};


// Функция для входа пользователя
const loginUsers = async (option) => await sendRequest(`${API}${ENDPOINTS.AUTH}${NESTED_AUTH_ENDPOINT.LOGIN}`, 'POST', option)


const sortProducts = (products) => {
    const sortWindow = document.querySelector('.sort-select')
    const sortValue = sortWindow?.value
    switch (true) {
        case sortValue === sortOption.higherPrice:
            console.log('sortToHigher')
            const sortToHigher = products.sort((a, b) => b.price - a.price)
            return renderProductsByCategory(main, sortToHigher)

        case sortValue === sortOption.lowerPrice:
            const sortToLower = products.sort((a, b) => a.price - b.price)
            return renderProductsByCategory(main, sortToLower)

        default :
            const sortByRank = products.sort((a, b) => a.rating.rate - b.rating.rate)
            return renderProductsByCategory(main, sortByRank)
    }
}


// Функция для удаления окна по нажатию клавиши
const removeWindowByKeyPress = (event, target) => {
    if (event.keyCode == '27') {  // Код клавиши ESC
        target.remove();
        document.removeEventListener('keydown', removeWindowByKeyPress);
    }
    console.log(event);
};

// Функция для создания формы входа
const createLoginForm = (target) => {
    let container = document.createElement('div');
    let labelLogin = document.createElement('label');
    let inputLogin = document.createElement('input');
    let labelPassword = document.createElement('label');
    let inputPassword = document.createElement('input');
    let submitButton = document.createElement('button');

    container.className = 'signup-container';
    labelLogin.setAttribute('for', 'loginInput');
    labelLogin.textContent = 'Login';

    inputLogin.setAttribute('id', 'loginInput');
    inputLogin.setAttribute('value', 'mor_2314');
    inputLogin.setAttribute('name', 'loginInput');
    inputLogin.setAttribute('placeholder', 'Enter login');

    labelPassword.setAttribute('for', 'passwordInput');
    labelPassword.textContent = 'Password';

    inputPassword.setAttribute('id', 'passwordInput');
    inputPassword.setAttribute('value', '83r5^_');
    inputPassword.setAttribute('type', 'password');
    inputPassword.setAttribute('name', 'passwordInput');
    inputPassword.setAttribute('placeholder', 'Enter password');

    submitButton.setAttribute('id', 'submitSignUpButton');
    submitButton.setAttribute('type', 'submit');
    submitButton.textContent = 'Submit';

    target.append(labelLogin, inputLogin, labelPassword, inputPassword, submitButton);

    submitButton.addEventListener('click', async (event) => {
        event.preventDefault();

        const loginInputValue = inputLogin.value;
        const passwordInputValue = inputPassword.value;

        const objUsers = {
            username: loginInputValue,
            password: passwordInputValue,
        };

        const objLogin = {
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(objUsers),
        };

        try {
            removeError();
            const user = await loginUsers(objLogin);
            console.log('token', user);

            if (!user) {
                throw new Error();
            }

            isUserLogin = true;

            const getAllUser = await getAllUsers();

            if (!getAllUser) {
                throw new Error();
            }

           userInfo =  getAllUser.find((user) => user.username === inputLogin.value);
            // {id: userId} =


            console.log('user', userInfo)

            const usersCarts = await getAllCarts();

            const {id: cartId} = usersCarts.find((cart) => cart.userId === userInfo.id);

            const {products} = await getSingleCart(cartId);

            const arrayOfProducts = await Promise.all(
                products.map(({productId}) => getProductById(productId))
            );


            const arrayOfProductsWithQuantity = arrayOfProducts.map((product) => {
                const productWithQuantity = products.find((p) => p.productId === product.id);
                return {
                    ...product,
                    quantity: productWithQuantity ? productWithQuantity.quantity : 0,
                };
            });

            if (userProducts !== null) {
                userProducts = [...userProducts, ...arrayOfProductsWithQuantity]
            } else {
                userProducts = arrayOfProductsWithQuantity
            }

            console.log('Updated userProducts:', arrayOfProductsWithQuantity);


            // Удаление модального окна после успешного логина
            const modalElement = document.querySelector('.modal');
            if (modalElement) {
                modalElement.remove();
            }

            // Отображение корзины с продуктами
            createModal(userProducts);

        } catch (error) {
            displayError('Ошибка при входе');
            console.error('Не удалось залогиниться', error);
        }
    });
};


// Функция для создания модального окна
const createModal = (position = modalWindowPosition.center) => {
    let modalDiv = document.createElement("div");
    let modalContent = document.createElement("div");
    let closeSpan = document.createElement("span");
    modalDiv.className = "modal";
    closeSpan.className = "close";
    closeSpan.textContent = "×";
    modalDiv.id = "myModal";


    switch (true) {
        case position === modalWindowPosition.right :
            modalContent.className = 'modal-content-right';
            break;
        case position === modalWindowPosition.center :
            modalContent.className = "modal-content";
            break;
    }


    modalContent.appendChild(closeSpan);
    modalDiv.appendChild(modalContent);

    document.addEventListener('keydown', (event) => {
        removeWindowByKeyPress(event, modalDiv);
    });

    closeSpan.addEventListener('click', () => modalDiv.remove());

    return {
        modalDiv, modalContent, closeSpan
    };
};


// Функция для создания модального окна с продуктом или ошибкой
const createModalWindow = (targetProduct, error) => {
    const {modalDiv, modalContent} = createModal();

    if (targetProduct) {
        let title = document.createElement("h2");
        let imgWrapper = document.createElement("div");
        let img = document.createElement("img");
        let description = document.createElement("p");
        let price = document.createElement("p");
        let ratingDiv = document.createElement("div");
        let rating = document.createElement("p");
        let count = document.createElement("p");

        title.textContent = targetProduct.title;

        imgWrapper.className = "modal-image-wrapper";

        img.src = targetProduct.image;
        img.alt = targetProduct.title;
        img.className = "modal-product-image";

        description.textContent = targetProduct.description;

        price.textContent = `Price: $${targetProduct.price}`;

        ratingDiv.className = "product-rating";

        rating.textContent = `Rating: ${targetProduct.rating.rate}`;

        count.textContent = `Count: ${targetProduct.rating.count}`;

        ratingDiv.appendChild(rating);
        ratingDiv.appendChild(count);
        modalContent.appendChild(title);
        imgWrapper.appendChild(img);
        modalContent.appendChild(imgWrapper);
        modalContent.appendChild(description);
        modalContent.appendChild(price);
        modalContent.appendChild(ratingDiv);
    }

    if (error) {
        let title = document.createElement("h2");
        title.textContent = "Error";
        modalContent.appendChild(title);
    }

    // Добавляем элементы на страницу
    body.append(modalDiv);
};


// Функция для отображения продуктов по категории
const renderProductsByCategory = (target, products) => {
    target.innerHTML = '';
    const cards = products.map((product) => {
        let productDiv = document.createElement("div");
        let title = document.createElement("h2");
        let img = document.createElement("img");
        let category = document.createElement("p");
        let price = document.createElement("p");
        let ratingDiv = document.createElement("div");
        let rating = document.createElement("p");
        let wishButton = document.createElement("button");


        productDiv.addEventListener('click', async () => {
            try {
                const targetProduct = await getProductById(product.id);
                createModalWindow(targetProduct);
            } catch (error) {
                createModalWindow(null, error);
                console.error('Не удалось получить продукт для категории: ', product.id);
            }
        });

        // Устанавливаем атрибуты и содержимое
        productDiv.className = "product";
        productDiv.id = `product-${product.id}`;

        title.className = "product-title";
        title.textContent = product.title;

        img.className = "product-image";
        img.src = product.image;
        img.alt = product.title;

        category.className = "product-category";
        category.textContent = `Category: ${product.category}`;

        price.className = "product-price";
        price.textContent = `Price: $${product.price}`;

        rating.className = "starability-result";
        rating.setAttribute("data-rating", Math.round(product.rating.rate));

        ratingDiv.className = "product-rating";

        wishButton.className = "wish-button";

        wishButton.textContent = '🩶';

        wishButton.addEventListener('click', async (event) => {
            event.stopPropagation();


            // Проверяем, существует ли продукт в корзине
            const existingProduct = userProducts?.find((p) => p.id === product.id);


            if (existingProduct) {
                displayError('Этот товар уже добавлен в корзину.');
                return;
            }


            const updatedProduct = {
                ...product,
                quantity: 1
            }


            // Добавляем продукт в корзину

            if (userProducts === null) {
                userProducts = [updatedProduct]
            } else {
                userProducts.push(updatedProduct);
                const cartQuantity = document.querySelector('.cart-quantity');
                if (cartQuantity) {
                    cartQuantity.textContent = `Items ${userProducts.length}`;
                }
            }


            console.log('Продукт добавлен в корзину 🙂', product);


        });


        // Добавляем элементы
        ratingDiv.appendChild(rating);
        productDiv.appendChild(title);
        productDiv.appendChild(img);
        productDiv.appendChild(category);
        productDiv.appendChild(price);
        productDiv.appendChild(ratingDiv);
        productDiv.appendChild(wishButton);
        return productDiv;
    });

    target.append(...cards);

};


const createModalCart = (products) => {
    const {modalDiv, modalContent} = createModal();
    renderProductsByCategory(modalContent, products)
    modalDiv.append(modalContent)
}


// Функция для отображения категорий
const renderCategories = async () => {
    const container = document.createElement('div');
    container.className = 'products-container';


    const navigation = document.createElement('nav');

    // Создаем контейнер для гифки
    const gif = document.createElement('div');
    gif.className = 'shop-gif';

    // Встраиваем гифку из Tenor
    gif.innerHTML = `<iframe src="https://tenor.com/embed/13137850" frameborder="0" allowfullscreen></iframe>`;


    const buttonContainer = document.createElement('div')
    buttonContainer.className = 'button-container';


    const loginButton = document.createElement('button');
    loginButton.className = 'login-button';
    loginButton.textContent = '👨‍💻';
    loginButton.innerHTML = '<img src="img/709722.svg" alt="Cart Icon" width="24" height="24" />';


    const cartButton = document.createElement('button');
    cartButton.className = 'cart-button';
    cartButton.innerHTML = '<img src="img/34568.svg" alt="Cart Icon" width="24" height="24" />';


    const sortWidnow = document.createElement('select');
    sortWidnow.className = 'sort-select'
    sortWidnow.addEventListener('change', () => {
        console.log('allProducts', allProducts)
        sortProducts(allProducts)
    })

    for (let key in sortOption) {
        const option = document.createElement('option');
        option.className = 'sort-option';
        option.textContent = sortOption[key]
        sortWidnow.appendChild(option)
    }


    const list = document.createElement('ul');

    try {
        const categories = await getAllCategories();


        const allItems = document.createElement('li');
        allItems.className = 'all-items';
        allItems.textContent = 'All';

        allItems.addEventListener('click', async () => {
            const products = await getAllProducts();
            console.log(products);
            allProducts = products;
            renderProductsByCategory(main, products);
        });


        const itemArray = categories.map((category) => {
            const item = document.createElement('li');
            item.textContent = category;

            item.addEventListener('click', async () => {
                const products = await getProductsByCategory(category);
                console.log(products);
                allProducts = products
                renderProductsByCategory(main, products);
            });

            return item;
        });

        loginButton.addEventListener('click', () => {
            const {modalDiv, modalContent} = createModal();
            createLoginForm(modalContent);
            body.append(modalDiv);

        });

        cartButton.addEventListener('click', () => {
            const {modalDiv, modalContent} = createModal(modalWindowPosition.right);
            if (userProducts?.length === 0 || userProducts === null) {
                const message = document.createElement('p');
                message.className = 'notification-message';
                message.textContent = 'Cart is empty';
                modalContent.append(message);
            } else {
                const cards = userProducts?.map((product) => {
                    let productDiv = document.createElement("div");
                    let title = document.createElement("h2");
                    let img = document.createElement("img");
                    let category = document.createElement("p");
                    let price = document.createElement("p");
                    let ratingDiv = document.createElement("div");
                    let rating = document.createElement("p");
                    let wishButton = document.createElement("button");

                    let quantityWrapper = document.createElement("div");
                    let plusButton = document.createElement("button");
                    let quantityWrapperSpan = document.createElement("span")
                    let minusButton = document.createElement("button");

                    // Устанавливаем атрибуты и содержимое
                    productDiv.className = "product";
                    productDiv.id = `product-${product.id}`;

                    title.className = "product-title";
                    title.textContent = product.title;

                    img.className = "product-image";
                    img.src = product.image;
                    img.alt = product.title;

                    category.className = "product-category";
                    category.textContent = `Category: ${product.category}`;

                    price.className = "product-price";
                    price.textContent = `Price: $${product.price}`;

                    wishButton.className = "wish-button";
                    wishButton.textContent = '🩶';

                    quantityWrapper.className = "quantity-wrapper"
                    plusButton.className = "plus-button"
                    plusButton.textContent = "➕"

                    quantityWrapperSpan.className = "quantity-wrapper-span"
                    quantityWrapperSpan.textContent = `${product.quantity}`
                    minusButton.className = "minus-button"
                    minusButton.textContent = "➖"


                    plusButton.addEventListener('click', (event) => {
                        console.log('ddd', userProducts)
                        product.quantity = product.quantity + 1
                        quantityWrapperSpan.textContent = product.quantity
                        console.log(product.quantity)

                        const totalPrice = countTotalPrice(userProducts);
                        totalAmount.textContent = `${totalPrice}`;
                    })

                    minusButton.addEventListener('click', (event) => {
                        if (product.quantity > 1) {
                            product.quantity = product.quantity - 1
                            quantityWrapperSpan.textContent = product.quantity
                        }
                        const totalPrice = countTotalPrice(userProducts);
                        totalAmount.textContent = `${totalPrice}`;


                    })

                    wishButton.addEventListener('click', async (event) => {
                        event.stopPropagation();
                        try {
                            const carts = await getAllCarts();
                            console.log('Cart', carts);
                        } catch (error) {
                            console.log('Ошибка', error);
                        }
                    });

                    // Добавляем элементы
                    ratingDiv.appendChild(rating);
                    productDiv.appendChild(title);
                    productDiv.appendChild(img);
                    productDiv.appendChild(category);
                    productDiv.appendChild(price);
                    productDiv.append(wishButton, quantityWrapper);
                    quantityWrapper.append(minusButton, quantityWrapperSpan, plusButton)
                    return productDiv;

                });


                const header = document.createElement('div');
                header.className = 'cart-header';

                const content = document.createElement('div');
                content.className = 'cart-content';

                const footer = document.createElement('div')
                footer.className = 'cart-footer'

                const title = document.createElement('h2');
                title.className = 'cart-title';
                title.textContent = 'Your  Cart';

                const cartQuantity = document.createElement('span');
                cartQuantity.className = 'cart-quantity';
                cartQuantity.textContent = `Items: ${userProducts ? userProducts.length : '0'}`;

                const orderButton = document.createElement('button');
                orderButton.className = 'order-button';
                orderButton.textContent = 'Order'

                orderButton.addEventListener('click', () => {
                    const totalAmount = countTotalPrice(userProducts);
                    const modalDiv = createOrderModal(userProducts, totalAmount);
                    document.body.append(modalDiv);
                });


                const totalAmount = document.createElement('span');
                totalAmount.className = 'amount-span';
                totalAmount.textContent = countTotalPrice(userProducts); // Подсчет общей суммы


                header.append(title);
                header.append(cartQuantity);
                content.append(...cards)
                footer.append(orderButton, totalAmount)

                modalContent.append(header, content, footer)


            }


            body.append(modalDiv);
        });


        list.append(...itemArray);
        buttonContainer.append(loginButton, cartButton)
        navigation.prepend(list, buttonContainer, gif);
        container.append(navigation, sortWidnow);
        list.prepend(allItems)
        body.prepend(container);




        // Создаем контейнер для формы подписки
        const subscribeContainer = document.createElement('div');
        subscribeContainer.className = 'subscribe-container';

// Добавляем заголовок
        const subscribeTitle = document.createElement('h3');
        subscribeTitle.textContent = 'Subscribe to our newsletter';
        subscribeContainer.appendChild(subscribeTitle);

// Добавляем описание (если нужно)
        const subscribeDescription = document.createElement('p');
        subscribeDescription.textContent = 'Receive news and updates directly to your Gmail.';
        subscribeContainer.appendChild(subscribeDescription);

// Создаем форму
        const subscribeForm = document.createElement('form');
        subscribeForm.className = 'subscribe-form';

// Поле ввода для email
        const emailInput = document.createElement('input');
        emailInput.type = 'email';
        emailInput.placeholder = 'Enter your email';
        emailInput.required = true;
        emailInput.className = 'email-input';
        subscribeForm.appendChild(emailInput);

// Кнопка отправки
        const subscribeButton = document.createElement('button');
        subscribeButton.type = 'submit';
        subscribeButton.textContent = 'Subscribe';
        subscribeButton.className = 'subscribe-button';
        subscribeForm.appendChild(subscribeButton);

// Добавляем форму в контейнер
        subscribeContainer.appendChild(subscribeForm);

// Вставляем контейнер перед футером
        body.appendChild(subscribeContainer);


        // Создаем контейнер футера
        const footerProductsContainer = document.createElement('div');
        footerProductsContainer.className = 'footer-products-container';

// Добавляем текст футера
        const footerText = document.createElement('p');
        footerText.textContent = '© 2024 Your Store. All Rights Reserved.';

// Создаем контейнер для социальных ссылок
        const socialLinks = document.createElement('div');
        socialLinks.className = 'footer__social';

// Добавляем социальные ссылки
        const facebookLink = document.createElement('a');
        facebookLink.href = 'https://www.facebook.com';
        facebookLink.target = '_blank';
        facebookLink.setAttribute('aria-label', 'Facebook');
        facebookLink.innerHTML = '<i class="fab fa-facebook-f"></i> Facebook';

        const twitterLink = document.createElement('a');
        twitterLink.href = 'https://www.twitter.com';
        twitterLink.target = '_blank';
        twitterLink.setAttribute('aria-label', 'Twitter');
        twitterLink.innerHTML = '<i class="fab fa-twitter"></i> Twitter';

        const instagramLink = document.createElement('a');
        instagramLink.href = 'https://www.instagram.com';
        instagramLink.target = '_blank';
        instagramLink.setAttribute('aria-label', 'Instagram');
        instagramLink.innerHTML = '<i class="fab fa-instagram"></i> Instagram';

        socialLinks.appendChild(facebookLink);
        socialLinks.appendChild(twitterLink);
        socialLinks.appendChild(instagramLink);

// Создаем контейнер для дополнительных ссылок
        const footerLinks = document.createElement('div');
        footerLinks.className = 'footer__links';

// Добавляем дополнительные ссылки
        // Создание ссылки "Delivery & Payment" с иконкой грузовичка
        const deliveryLink = document.createElement('a');
        deliveryLink.href = '#';
        deliveryLink.innerHTML = '<i class="fas fa-truck"></i> Delivery & Payment';

// Создание ссылки "Returns" с иконкой таймера
        const returnsLink = document.createElement('a');
        returnsLink.href = '#';
        returnsLink.innerHTML = '<i class="fas fa-undo-alt"></i> Returns';

// Создание ссылки "Contact Us" с иконкой телефона
        const contactLink = document.createElement('a');
        contactLink.href = '#';
        contactLink.innerHTML = '<i class="fas fa-phone"></i> Contact Us';

// Создание ссылки "Blog" с иконкой книги
        const blogLink = document.createElement('a');
        blogLink.href = '#';
        blogLink.innerHTML = '<i class="fas fa-book"></i> Blog';
        footerLinks.appendChild(deliveryLink);
        footerLinks.appendChild(returnsLink);
        footerLinks.appendChild(contactLink);
        footerLinks.appendChild(blogLink);

// Добавляем все элементы в футер
        footerProductsContainer.appendChild(footerText);
        footerProductsContainer.appendChild(socialLinks);
        footerProductsContainer.appendChild(footerLinks);

// Вставляем футер в тело документа
        document.body.appendChild(footerProductsContainer);


    } catch (error) {
        console.error('Ошибка при загрузке категотрий', error);
    }
}


// Функция для подсчета общей стоимости товаров в корзине
function countTotalPrice(products) {
    const total = products.reduce((acc, product) => acc + product.price * product.quantity, 0);
    return `$${total.toFixed(2)}`;
}


function createOrderModal() {
    const overlay = document.createElement('div');
    overlay.classList.add('modal-overlay');

    const modalContainer = document.createElement('div');
    modalContainer.classList.add('modal-container');

    // Header with title
    const modalTitle = document.createElement('h2');
    modalTitle.textContent = 'Placing an order';

    // Form for user details
    const form = document.createElement('form');

    const nameLabel = document.createElement('label');
    nameLabel.textContent = 'Name:';
    nameLabel.setAttribute('for', 'nameInput');

    const nameInput = document.createElement('input');
    nameInput.type = 'text';
    nameInput.id = 'nameInput';
    nameInput.name = 'name';

    const addressLabel = document.createElement('label');
    addressLabel.textContent = 'Delivery address:';
    addressLabel.setAttribute('for', 'addressInput');

    const addressInput = document.createElement('input');
    addressInput.type = 'text';
    addressInput.id = 'addressInput';
    addressInput.name = 'address';

    const phoneLabel = document.createElement('label');
    phoneLabel.textContent = 'Phone number:';
    phoneLabel.setAttribute('for', 'phoneInput');

    const phoneInput = document.createElement('input');
    phoneInput.type = 'tel';
    phoneInput.id = 'phoneInput';
    phoneInput.name = 'phone';

    const submitButton = document.createElement('button');
    submitButton.type = 'submit';
    submitButton.textContent = 'Place an order';

    form.append(nameLabel, nameInput, addressLabel, addressInput, phoneLabel, phoneInput, submitButton);

    // Close button
    const closeButton = document.createElement('span');
    closeButton.classList.add('close-button');
    closeButton.textContent = '×';

    closeButton.addEventListener('click', () => {
        overlay.remove();
    });

    // Container for products and total price
    const productsContainer = document.createElement('div');
    productsContainer.classList.add('products-container');

    const productsHeader = document.createElement('h3');
    productsHeader.textContent = 'Products in your cart';
    productsContainer.appendChild(productsHeader);

    userProducts.forEach(product => {
        const productDiv = document.createElement('div');
        productDiv.classList.add('order-product');

        const img = document.createElement('img');
        img.src = product.image;
        img.alt = product.title;
        img.classList.add('order-product-image');

        const title = document.createElement('p');
        title.textContent = product.title;

        const quantity = document.createElement('p');
        quantity.textContent = `Quantity:  ${product.quantity}`;

        const price = document.createElement('p');
        price.textContent = ` Price: $${(product.price * product.quantity).toFixed(2)}`;

        productDiv.append(img, title, quantity, price);
        productsContainer.appendChild(productDiv);
    });

    // Total amount
    const totalAmount = document.createElement('p');
    totalAmount.classList.add('order-total');
    totalAmount.textContent = `Total Amount: ${countTotalPrice(userProducts)}`;

    modalContainer.append(modalTitle, form, productsContainer, totalAmount, closeButton);
    overlay.append(modalContainer);

    return overlay;
}


// Создаем основной контейнер dropdown
const dropdown = document.createElement('div');
dropdown.className = 'dropdown';

// Создаем кнопку dropdown
const dropbtn = document.createElement('button');
dropbtn.className = 'dropbtn';
dropbtn.textContent = 'More Links';

// Добавляем кнопку в контейнер dropdown
dropdown.appendChild(dropbtn);

// Создаем контейнер для содержимого dropdown
const dropdownContent = document.createElement('div');
dropdownContent.className = 'dropdown-content';

// Создаем ссылки в dropdown
const links = [
    { href: '#', iconClass: 'fas fa-info-circle', text: 'About Us' },
    { href: '#', iconClass: 'fas fa-gift', text: 'Promotions' },
    { href: '#', iconClass: 'fas fa-newspaper', text: 'News' },
    { href: '#', iconClass: 'fas fa-user-cog account-settings', text: 'Account Settings' }
];

// Добавляем ссылки в dropdownContent
// Добавляем ссылки в dropdownContent
links.forEach(linkInfo => {
    const link = document.createElement('a');
    link.href = linkInfo.href;

    const icon = document.createElement('i');
    icon.className = linkInfo.iconClass;

    link.appendChild(icon);
    link.appendChild(document.createTextNode(` ${linkInfo.text}`));

    dropdownContent.appendChild(link);

    // Добавляем обработчик события на ссылку "Account Settings"
    if (linkInfo.text === 'Account Settings') {
        link.addEventListener('click', (event) => {
            event.preventDefault();
            console.log('Account Settings clicked');

            // Проверка на то, что пользователь залогинен
            if (!isUserLogin) {
                // Создание и отображение маленького модального окна с сообщением
                const modalDiv = document.createElement('div');
                const modalContent = document.createElement('div');

                modalDiv.className = 'small-modal-overlay';
                modalContent.className = 'small-modal-content';

                const title = document.createElement('h2');
                title.textContent = 'Access Denied';
                modalContent.appendChild(title);

                const message = document.createElement('p');
                message.textContent = 'Please log in to access Account Settings.';
                modalContent.appendChild(message);

                const closeButton = document.createElement('button');
                closeButton.textContent = 'Close';
                closeButton.addEventListener('click', () => {
                    document.body.removeChild(modalDiv);
                });
                modalContent.appendChild(closeButton);

                modalDiv.appendChild(modalContent);
                document.body.appendChild(modalDiv);
                return; // Не открывать модальное окно "Account Settings"
            }

            // Здесь вызов модального окна "Account Settings"
            const modalDiv = document.createElement('div');
            const modalContent = document.createElement('div');

            modalDiv.className = 'modal-overlay';
            modalContent.className = 'modal-content';

            const title = document.createElement('h2');
            title.textContent = 'Account Settings';
            modalContent.appendChild(title);

            const inputGroup = document.createElement('div');

            // Username
            const usernameLabel = document.createElement('label');
            usernameLabel.textContent = 'Username:';

            const usernameInputContainer = document.createElement('div');
            usernameInputContainer.className = 'input-container'; // Присвоение класса контейнеру

            const usernameInput = document.createElement('input');
            usernameInput.type = 'text';
            usernameInput.name = 'username';
            usernameInput.disabled = true;
            usernameInput.value = userInfo.name.firstname;

            const usernameEditIcon = document.createElement('i');
            usernameEditIcon.className = 'fas fa-edit edit-icon'; // Присвоение класса иконке

            usernameInputContainer.appendChild(usernameInput);
            usernameInputContainer.appendChild(usernameEditIcon);
            inputGroup.appendChild(usernameLabel);
            inputGroup.appendChild(usernameInputContainer);

            // Last Name
            const lastNameLabel = document.createElement('label');
            lastNameLabel.textContent = 'Lastname:';

            const lastNameInputContainer = document.createElement('div');
            lastNameInputContainer.className = 'input-container'; // Присвоение класса контейнеру

            const lastNameInput = document.createElement('input');
            lastNameInput.type = 'text'; // Changed type to 'text'
            lastNameInput.name = 'lastname';
            lastNameInput.disabled = true;
            lastNameInput.value = userInfo.name.lastname;

            const lastNameEditIcon = document.createElement('i');
            lastNameEditIcon.className = 'fas fa-edit edit-icon'; // Присвоение класса иконке

            lastNameInputContainer.appendChild(lastNameInput);
            lastNameInputContainer.appendChild(lastNameEditIcon);
            inputGroup.appendChild(lastNameLabel);
            inputGroup.appendChild(lastNameInputContainer);

            // Phone
            const phoneLabel = document.createElement('label');
            phoneLabel.textContent = 'Phone:';

            const phoneInputContainer = document.createElement('div');
            phoneInputContainer.className = 'input-container'; // Присвоение класса контейнеру

            const phoneInput = document.createElement('input');
            phoneInput.type = 'text'; // Changed type to 'text'
            phoneInput.name = 'phone';
            phoneInput.disabled = true;
            phoneInput.value = userInfo.phone;

            const phoneEditIcon = document.createElement('i');
            phoneEditIcon.className = 'fas fa-edit edit-icon'; // Присвоение класса иконке

            phoneInputContainer.appendChild(phoneInput);
            phoneInputContainer.appendChild(phoneEditIcon);
            inputGroup.appendChild(phoneLabel);
            inputGroup.appendChild(phoneInputContainer);

            // Address (City)
            const addressLabel = document.createElement('label');
            addressLabel.textContent = 'City:';

            const addressInputContainer = document.createElement('div');
            addressInputContainer.className = 'input-container'; // Присвоение класса контейнеру

            const addressInput = document.createElement('input');
            addressInput.type = 'text';
            addressInput.name = 'city';
            addressInput.disabled = true;
            addressInput.value = userInfo.address.city;

            const addressEditIcon = document.createElement('i');
            addressEditIcon.className = 'fas fa-edit edit-icon'; // Присвоение класса иконке

            addressInputContainer.appendChild(addressInput);
            addressInputContainer.appendChild(addressEditIcon);
            inputGroup.appendChild(addressLabel);
            inputGroup.appendChild(addressInputContainer);

            const saveButton = document.createElement('button');
            saveButton.textContent = 'Save';
            saveButton.addEventListener('click', () => {
                document.body.removeChild(modalDiv);
            });
            inputGroup.appendChild(saveButton);

            modalContent.appendChild(inputGroup);

            modalDiv.appendChild(modalContent);
            document.body.appendChild(modalDiv);
        });
    }
});

dropdown.appendChild(dropdownContent);
document.body.appendChild(dropdown);
dropdown.appendChild(dropdownContent);
document.body.appendChild(dropdown);
// Инициализация загрузки продуктов и категорий при загрузке страницы
getAllProducts();
getAllCategories();
renderCategories();
