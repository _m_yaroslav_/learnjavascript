// const createStudent = function (name, age, grade) {
//     return {
//         studentName: name,
//         studentAge: age,
//         studentGrade: grade,
//     }
//
// }

// ОДНО И ТОЖЕ ФУНКЦИЯ

const createStudent = (name, age, grade) => ({name, age, grade})
// ОДНО И ТОЖЕ ФУНКЦИЯ


console.log(createStudent)

const student1 = createStudent('Yaroslav', 17, [7, 10, 8])
const student2 = createStudent('Kolya', 15, [11, 10, 10])
const student3 = createStudent('Anna', 16, [9, 5, 9])
const student4 = createStudent('Stas', 14, [10, 11, 10])
const student5 = createStudent('Nastya', 17, [11, 11, 9])
const student6 = createStudent('Grisha', 18, [9, 8, 7])
const student7 = createStudent('Alex', 19, [11, 8, 7])
const student8 = createStudent('Tom', 15, [9, 6, 11])
const student9 = createStudent('Mark', 14, [12, 9, 6])
const student10 = createStudent('Maksim', 16, [11, 12, 12])


const group = {
    students: [],


    addStudent: function (student) {
        student.forEach(student => this.students.push(student))
    },


    get showStudents() {
        // this.student.forEach( (student, index, array) => {
        //     console.log(`Student ${student.name}, age ${student.age}, grade ${student.grade}`)
        //
        // })

        this.students.forEach(({name, age, grade}) => {
            console.log(`Студент ${name}, ${age} років, оцінки: ${grade}`);
        });

        console.log(this.students)
    },

    calculateAverageGrade: function () {
        let totalStudents = this.students.length;
        let totalGradeSum = 0;

        this.students.forEach(({grade}) => {
            totalGradeSum += grade.reduce((acc, cur) => acc + cur, 0) / grade.length;


        })
        //     let gradeSum = grade.reduce((acc, cur) => acc + cur, 0);
        //
        //     let averageGrade = gradeSum / grade.length;
        //     totalGradeSum += averageGrade
        //
        //
        console.log(Math.round(totalGradeSum / totalStudents))

    },


    printStudentsWithHightGrades: function (mingrade) {
        this.students.forEach(({name, grade}) => {
            let gradeSum = grade.reduce((acc, cur) => acc + cur, 0) / grade.length
            if (gradeSum >= mingrade) {
                console.log(`Студент ${name}, ${grade}`)

            }
        })
    },
}

group.addStudent([student1, student2, student3, student4, student5, student6, student7, student8, student9, student10])
group.showStudents
group.calculateAverageGrade();
group.printStudentsWithHightGrades(9)



