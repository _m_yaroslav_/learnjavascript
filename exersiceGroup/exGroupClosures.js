const students = {

}


const createStudent = (name, age, grade) => ({name, age, grade})



createStudent()

const createGroup = () => {
    const students = []

    const showStudent = () => [...students]



    const addStudent = (student) => {
        students.push(student)
    }



    const addStudents = (arrayStudents) => {
        arrayStudents.forEach((student) => {
            students.push(student);
        });
    }


    const calculateAverageGrade =() => {
        let totalStudents = students.length;
        let totalGradeSum = 0;
        students.forEach(({grade}) => {
            totalGradeSum += grade.reduce((acc, cur ) => acc + cur, 0) /grade.length
        })
        console.log(Math.round(totalGradeSum / totalStudents))

    }

    const minAvarageGrade = () => {
students.forEach(({name, grade}) => {
    let gradeSum = grade.reduce((acc, cur) => acc + cur, 0) / grade.length
    if (gradeSum >= 9) {
        console.log(`Студент ${name}, ${grade}`)

    }
})
    }


    return {
        showStudent,
        addStudent,
        addStudents,
        calculateAverageGrade,
        minAvarageGrade

    }

}

const student1 = createStudent('Yaroslav', 17, [7, 10, 8])
const student2 = createStudent('Kolya', 15, [11, 10, 10])
const student3 = createStudent('Anna', 16, [9, 5, 9])
const student4 = createStudent('Stas', 14, [10, 11, 10])
const student5 = createStudent('Nastya', 17, [11, 11, 9])
const student6 = createStudent('Grisha', 18, [9, 8, 7])
const student7 = createStudent('Alex', 19, [11, 8, 7])
const student8 = createStudent('Tom', 15, [9, 6, 11])
const student9 = createStudent('Mark', 14, [12, 9, 6])
const student10 = createStudent('Maksim', 16, [11, 12, 12])




const PE_104 = createGroup()
PE_104.addStudent(createStudent('Kolya', 23, [11,12,11]))

console.log(PE_104.showStudent())


const PE_105 = createGroup()
PE_105.addStudent(createStudent('Yaroslav', 18, [10,12,11]))

console.log(PE_105.showStudent())


const PE_106 = createGroup()
PE_106.addStudents([student1, student2, student3, student4, student5, student6, student7, student8, student9, student10]);
console.log(PE_106.showStudent())


PE_106.calculateAverageGrade()
PE_106.minAvarageGrade()