// const obj = {}
// obj.name = 'Kolya'
//
// console.log(obj.name)
//
// obj.name = 'Yarik';
//
// console.log(obj.name)
// console.log(obj['name'])


// const USD = 'usd';
// const EUR = 'eur';

// const currencyRate = {
//     usd: 40,
//     eur: 50,
//
// }

// console.log(currencyRate[USD])


// const currency = {
//     usd: 'usd',
//     euro: 'euro',
//     "potand zlotiy": 'zlotiy',
// }
//
// currencyRate.zlotiy = 'potand zlotiy'
// currencyRate.zlotiy = 10;
//
// console.log(currencyRate[currency['potand zlotiy']])

// console.log(currencyRate)
// console.log(currencyRate[currency.usd])


// for (key in currencyRate) {
//
//     console.log(`Key ${key} has value ${currencyRate[key]}`)
// }


// const exchanger = {
//     amount : 0,
//     rate: {
//         usd: 'usd',
//         euro: 'euro',
//         gbp: 'gbp',
//         zlotiy: 'zlotiy',
//     },
//
//      toUSD: function() {
//
//         console.log(this);
// }
//
// }
//
// exchanger.toUSD()


// HW OBJECTS

// Обработчик изменения выбора валюты
const select = document.querySelector('#currency');

select.addEventListener('change', (event) => {
    const currency = event.target.value.toLowerCase();
    console.log(event.target.value);
});

// Функция для создания селектора валюты
function createCurrencySelector(objCurrency, target) {

    for (key in objCurrency) {
        const option = document.createElement('option');
        option.textContent = objCurrency[key].toUpperCase()
        target.append(option);
    }
}




// Объект с валютами
const currency = {
    uah: 'uah',
    usd: 'usd',
    eur: 'eur',
    zlotiy: 'zlotiy',
    gbp: 'gbp',
}

// Объект кошелька
const wallet = {
    currency: {
        uah: 0,
        usd: 0,
        eur: 0,
        zlotiy: 0,
    },
    rates: {
        uah: {usd: 39.33, eur: 42.33, zlotiy: 9.87},
        usd: {uah: 0.025, eur: 0.93, zlotiy: 3.98},
        eur: {uah: 0.024, usd: 1.08, zlotiy: 4.29},
        zlotiy: {uah: 0.10, usd: 0.25, eur: 0.23},
    },

    balance: function () {
        console.group('Currency balance')
        for (let currencyName in this.currency) {
            console.log(`${currencyName} : ${this.currency[currencyName]}`);
        }
        console.groupEnd();
    },

    deposite: function (currency, amount) {
        console.group('Deposite');
        if (currency in this.currency) {
            this.currency[currency] += amount;

            this.transaction.push(`${Date.now()} We put ${amount} in ${currency}`);
            console.log(`You successfully deposit ${amount} ${currency} `)
            console.log(`You total balance for ${this.currency[currency]}`)
        } else {
            console.log(`You don't have ${currency} in your wallet`)
        }
        console.groupEnd();
    },

    withdrawal: function (currency, amount) {
        console.group('Withdraw')
        if (currency in this.currency) {
            this.currency[currency] -= amount;
            this.transaction.push(`${Date.now()} We put ${amount} in ${currency}`);
            console.log(`You successfully withdraw ${amount} ${currency}`);
            console.log(`You total balance for ${this.currency[currency]}`)
        }
        console.groupEnd('Withdraw')
    },

    getTimeStamp: function () {
        console.log(this.transaction.map(item => item.split(" ")[0]))
    },

    transaction: [],
    transactionHistory: function () {
        console.group('Transaction history');
        for (let i = 0; i < this.transaction.length; i++) {
            console.log(`${this.transaction[i]}`);
        }
        console.groupEnd();
    },
}


// Обработчик кнопки для депозита
const depositButton = document.querySelector("#deposite-button")
depositButton.addEventListener('click', () => {

    wallet.deposite(select.value.toLowerCase(), Number(inputAmount.value))

});


// Обработчик кнопки для вывода
const withdrawButton = document.querySelector("#withdrawal-button")
withdrawButton.addEventListener('click', () => {

    wallet.withdrawal(select.value.toLowerCase(), Number(inputWithdrawal.value))

});


// Обработчик кнопки для баланса
const balanceButton = document.querySelector("#showBalance-button")
balanceButton.addEventListener('click', () => {
    wallet.balance()

});




// Получение элементов DOM
const inputAmount = document.querySelector("#inputAmount");

const inputWithdrawal = document.querySelector("#inputWithdrawal");


// Вызов функции для создания селектора валюты
createCurrencySelector(currency, select);

// Вызов методов объекта wallet

// Пользовательский ввод для депозита
// wallet.deposite(prompt('Введите какую валюту хотите положить на депозит'), +prompt('Введите количество'));
// wallet.deposite(currency.uah, 200);

// wallet.withdrawal(currency, amount );
// Вывод истории транзакций
// wallet.transactionHistory();

// Вывод временных меток транзакций
// wallet.getTimeStamp();

