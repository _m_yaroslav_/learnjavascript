// function generationPassword(length) {
//
//     const charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_+';
//     let password = "";
//
//     for (let i = 0; i < length; i++) {
//
//
//         const randomIndex = Math.floor(Math.random() * charset.length)
//
//         password += charset[randomIndex]
//
//         console.log(charset[randomIndex])
//         }
//     console.log(password)
//
// }
//
// generationPassword(5)


function countVowels(string) {

    const vowelsLetters = 'aeiouAEIOU';

    let countingResult = 0;

    for (let i = 0; i < string.length; i++) {

        let char = string[i] //чтобы хранить текущий символ строки

        if (vowelsLetters.includes(char)) {

            countingResult ++

        }
    }

    return countingResult


}
console.log(countVowels('hello')); // 2
console.log(countVowels('age')); // 2
console.log(countVowels('ball')); // 1



